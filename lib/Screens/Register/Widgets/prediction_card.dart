import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:zyda/App/app_event.dart';
import 'package:zyda/BLoCs/places_bloc.dart';
import 'package:zyda/Models/prediction_model.dart';

class PredictionCard extends StatelessWidget {
  final Prediction prediction;

  const PredictionCard({Key key, this.prediction}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: () {
        context.read<PlacesBloc>().updatePlaceID(prediction.placeId);
        context.read<PlacesBloc>().add(GetPlace());
      },
      leading: Icon(Icons.location_on, color: Colors.grey),
      title: Text(
        prediction.description,
        style: TextStyle(color: Theme.of(context).accentColor, fontSize: 17),
      ),
      subtitle: Text(
        prediction.structuredFormatting.secondaryText,
        style: TextStyle(color: Colors.grey, fontSize: 14),
      ),
    );
  }
}
