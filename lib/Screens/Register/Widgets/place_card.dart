import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:zyda/Models/place_model.dart';

class PlaceCard extends StatefulWidget {
  final PlaceModel place;

  const PlaceCard({Key key, this.place}) : super(key: key);

  @override
  _PlaceCardState createState() => _PlaceCardState();
}

class _PlaceCardState extends State<PlaceCard> {
  Set<Marker> _markers = {};

  @override
  void initState() {
    super.initState();
    _markers.add(
      Marker(
        markerId: MarkerId('Place'),
        position: LatLng(widget.place.result.geometry.location.lat,
            widget.place.result.geometry.location.lng),
        infoWindow: InfoWindow(title: 'Address'),
        icon: BitmapDescriptor.defaultMarkerWithHue(
          BitmapDescriptor.hueRed,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        border: Border.all(color: Colors.grey),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 10),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            child: Text(
              widget.place.result.name,
              style: TextStyle(color: Colors.black, fontSize: 17),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: 180,
            child: GoogleMap(
              initialCameraPosition: CameraPosition(
                target: LatLng(widget.place.result.geometry.location.lat,
                    widget.place.result.geometry.location.lng),
                zoom: 11,
              ),
              gestureRecognizers: <Factory<OneSequenceGestureRecognizer>>[
                new Factory<OneSequenceGestureRecognizer>(
                  () => new EagerGestureRecognizer(),
                ),
              ].toSet(),
              mapToolbarEnabled: true,
              markers: _markers,
            ),
          ),
          SizedBox(height: 5),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
            child: ListTile(
              leading: SvgPicture.asset('assets/icn_details.svg'),
              title: Text(
                'Company Address',
                style: TextStyle(color: Colors.black, fontSize: 15),
              ),
              subtitle: Text(
                widget.place.result.formattedAddress,
                style: TextStyle(color: Colors.grey, fontSize: 13),
              ),
            ),
          ),
          widget.place.result.formattedPhoneNumber != null
              ? Padding(
                  padding: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                  child: ListTile(
                    leading: SvgPicture.asset('assets/icn_user.svg'),
                    title: Text(
                      'Company Reception',
                      style: TextStyle(color: Colors.black, fontSize: 15),
                    ),
                    subtitle: Text(
                      widget.place.result.formattedPhoneNumber,
                      style: TextStyle(color: Colors.grey, fontSize: 13),
                    ),
                  ),
                )
              : Container()
        ],
      ),
    );
  }
}
