import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:zyda/App/app_event.dart';
import 'package:zyda/App/app_state.dart';
import 'package:zyda/BLoCs/places_bloc.dart';
import 'package:zyda/Models/place_model.dart';
import 'package:zyda/Models/prediction_model.dart';
import 'package:zyda/Screens/Home/home.dart';
import 'package:zyda/Widgets/app_empty.dart';
import 'package:zyda/Widgets/app_loader.dart';
import 'package:zyda/Widgets/custom_appbar.dart';
import 'package:zyda/Widgets/custom_button.dart';
import 'package:zyda/Widgets/custom_text_field.dart';

import 'Widgets/place_card.dart';
import 'Widgets/prediction_card.dart';

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  final List<String> _steps = ['Location', 'Account', 'Verify'];
  int _current = 0;
  PlaceModel _selectedPlace;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(50),
        child: CustomAppBar(
          label: 'Sign In',
          actions: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              child: CustomButton(
                onTap: () {},
                label: 'Sign In',
                radius: 15,
                width: 80,
                gradient: LinearGradient(
                  colors: <Color>[Color(0xffF6D51F), Color(0xffF6A01F)],
                ),
              ),
            )
          ],
        ),
      ),
      backgroundColor: Colors.white,
      body: ListView(
        shrinkWrap: true,
        padding: EdgeInsets.symmetric(horizontal: 20),
        children: [
          SizedBox(height: 20),
          Row(
            children: List.generate(
              _steps.length,
              (index) {
                return Padding(
                  padding: const EdgeInsets.only(right: 15),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        _steps[index],
                        style: TextStyle(
                          color: index == _current
                              ? Theme.of(context).primaryColor
                              : Colors.grey,
                        ),
                      ),
                      SizedBox(height: 7),
                      Container(
                        height: 5,
                        width: 80,
                        decoration: BoxDecoration(
                          color: index == _current
                              ? Theme.of(context).primaryColor
                              : Colors.grey,
                          borderRadius: BorderRadius.circular(5),
                        ),
                      )
                    ],
                  ),
                );
              },
            ),
          ),
          SizedBox(height: 20),
          Row(
            children: [
              SvgPicture.asset('assets/icn_company.svg'),
              SizedBox(width: 10),
              Text('Set your work location',
                  style: TextStyle(fontSize: 18, color: Colors.black54))
            ],
          ),
          SizedBox(height: 20),
          _selectedPlace == null
              ? CustomTextField(
                  label: 'Work Address',
                  icon: Icons.search,
                  onChange: (String v) {
                    context.read<PlacesBloc>().updateInput(v);
                    context.read<PlacesBloc>().add(Click());
                  },
                )
              : Container(),
          SizedBox(height: _selectedPlace == null ? 20 : 0),
          BlocListener<PlacesBloc, AppState>(
            listener: (_, state) {
              if (state is GetPlaceDone) {
                setState(() {
                  _selectedPlace = state.response as PlaceModel;
                });
              }
            },
            child: BlocBuilder<PlacesBloc, AppState>(
              builder: (_, state) {
                if (state is Done) {
                  PredictionModel _res = state.response;
                  return ListView.separated(
                    shrinkWrap: true,
                    separatorBuilder: (_, index) {
                      return Divider(color: Colors.grey);
                    },
                    physics: ScrollPhysics(),
                    itemCount: _res.predictions.length,
                    itemBuilder: (_, index) {
                      return PredictionCard(
                        prediction: _res.predictions[index],
                      );
                    },
                  );
                } else if (state is Loading) {
                  return Padding(
                    padding: EdgeInsets.only(top: 80),
                    child: AppLoader(),
                  );
                } else if (state is Error) {
                  return Padding(
                    padding: EdgeInsets.only(top: 80),
                    child: AppEmpty(label: 'No Results Found'),
                  );
                } else if (state is GetPlaceDone) {
                  PlaceModel _place = state.response;
                  return PlaceCard(place: _place);
                }
                return Container();
              },
            ),
          ),
          SizedBox(height: 20),
          _selectedPlace != null
              ? CustomButton(
                  onTap: () => Navigator.push(
                      context, MaterialPageRoute(builder: (_) => Home())),
                  label: 'Next',
                  textColor: Colors.white,
                  gradient: LinearGradient(
                    colors: <Color>[
                      Theme.of(context).hoverColor,
                      Theme.of(context).primaryColor
                    ],
                  ),
                  radius: 10,
                )
              : Container(),
          _selectedPlace != null
              ? Padding(
                  padding: const EdgeInsets.only(top: 20),
                  child: CustomButton(
                    onTap: () {
                      context.read<PlacesBloc>().add(Restart());
                      setState(() {
                        _selectedPlace = null;
                      });
                    },
                    radius: 10,
                    label: 'Search Again',
                    textColor: Theme.of(context).accentColor,
                    color: Colors.white,
                    borderColor: Theme.of(context).accentColor,
                  ),
                )
              : Container(),
        ],
      ),
    );
  }
}
