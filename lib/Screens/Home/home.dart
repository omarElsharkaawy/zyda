import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:zyda/App/app_event.dart';
import 'package:zyda/App/app_state.dart';
import 'package:zyda/BLoCs/home_bloc.dart';
import 'package:zyda/Models/AppModels/selectable_model.dart';
import 'package:zyda/Models/home_model.dart';
import 'package:zyda/Screens/Home/Widgets/delivery_widget.dart';
import 'package:zyda/Screens/Home/Widgets/meals_section.dart';
import 'package:zyda/Screens/Home/Widgets/order_by_widget.dart';
import 'package:zyda/Screens/Home/Widgets/section_view.dart';
import 'package:zyda/Widgets/app_loader.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List<SelectableModel> _meals = [];

  @override
  void initState() {
    _meals.add(SelectableModel(id: 0, title: 'All', selected: true));
    _meals.add(SelectableModel(id: 0, title: 'Syami', selected: false));
    _meals.add(SelectableModel(id: 0, title: 'Healthy', selected: false));
    context.read<HomeBloc>().add(Click());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 20),
        children: [
          SizedBox(height: 50),
          DeliveryWidget(),
          SizedBox(height: 30),
          Image.asset(
            'assets/meal.png',
            height: 100,
            alignment: Alignment.topLeft,
          ),
          Image.asset(
            'assets/banner.png',
            width: MediaQuery.of(context).size.width,
            fit: BoxFit.cover,
          ),
          SizedBox(height: 30),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              OrderByWidget(),
              Expanded(
                child: Align(
                  alignment: Alignment.centerRight,
                  child: MealsSection(meals: _meals),
                ),
              )
            ],
          ),
          BlocBuilder<HomeBloc, AppState>(
            builder: (_, state) {
              if (state is Done) {
                HomeModel _res = state.response;
                return ListView(
                  shrinkWrap: true,
                  physics: ScrollPhysics(),
                  children: [
                    SectionView(
                      image: 'assets/title_light.png',
                      meals: _res.light,
                    ),
                    SectionView(
                      image: 'assets/title_best_value.png',
                      meals: _res.bestValue,
                    ),
                    SectionView(
                      image: 'assets/title_plus.png',
                      meals: _res.plus,
                    ),
                  ],
                );
              }
              return Padding(
                padding: const EdgeInsets.only(top: 50),
                child: AppLoader(),
              );
            },
          )
        ],
      ),
    );
  }
}
