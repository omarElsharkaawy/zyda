import 'package:flutter/material.dart';
import 'package:zyda/Models/home_model.dart';

import 'meal_card.dart';

class SectionView extends StatelessWidget {
  final String image;
  final List<Meal> meals;

  const SectionView({Key key, this.image, this.meals}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      children: [
        Image.asset(
          image,
          height: 60,
          alignment: Alignment.topLeft,
        ),
        Container(
          height: 350,
          child: ListView.builder(
            shrinkWrap: true,
            physics: ScrollPhysics(),
            scrollDirection: Axis.horizontal,
            itemCount: meals.length,
            itemBuilder: (_, index) {
              return MealCard(meal: meals[index]);
            },
          ),
        ),
      ],
    );
  }
}
