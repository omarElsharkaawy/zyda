import 'package:flutter/material.dart';
import 'package:zyda/Models/AppModels/selectable_model.dart';

class MealsSection extends StatefulWidget {
  final List<SelectableModel> meals;

  const MealsSection({Key key, this.meals}) : super(key: key);

  @override
  _MealsSectionState createState() => _MealsSectionState();
}

class _MealsSectionState extends State<MealsSection> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 25,
      child: ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        physics: ScrollPhysics(),
        itemCount: widget.meals.length,
        itemBuilder: (_, index) {
          return InkWell(
            onTap: () {
              for (int i = 0; i < widget.meals.length; i++) {
                setState(() {
                  widget.meals[i].selected = false;
                });
              }
              setState(() {
                widget.meals[index].selected = true;
              });
            },
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 5),
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(7),
                  color: !widget.meals[index].selected
                      ? Colors.grey.withOpacity(.5)
                      : null,
                  gradient: widget.meals[index].selected
                      ? LinearGradient(
                          colors: <Color>[
                            Theme.of(context).hoverColor,
                            Theme.of(context).primaryColor
                          ],
                        )
                      : null,
                ),
                child: Text(
                  widget.meals[index].title,
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
