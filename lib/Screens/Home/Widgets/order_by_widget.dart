import 'package:flutter/material.dart';
import 'package:zyda/Helpers/text_helper.dart';

class OrderByWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Order By ${TextHelper().formatTime(time: DateTime.now())}',
          style: TextStyle(
            color: Theme.of(context).primaryColor,
            fontSize: 15,
          ),
        ),
        SizedBox(height: 5),
        Text(
          'Select Your Meal',
          style: TextStyle(
            color: Colors.grey,
            fontSize: 12,
          ),
        )
      ],
    );
  }
}
