import 'package:flutter/material.dart';
import 'package:zyda/Models/home_model.dart';

class MealCard extends StatelessWidget {
  final Meal meal;

  const MealCard({Key key, this.meal}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 20),
      child: Container(
        width: 125,
        child: ListView(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          children: [
            Align(
              alignment: Alignment.centerLeft,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20),
                child: Image.network(meal.img, height: 195, width: 125),
              ),
            ),
            SizedBox(height: 7),
            Text(
              'EGP ${meal.regularPrice}',
              textAlign: TextAlign.center,
              style: TextStyle(
                decoration: TextDecoration.lineThrough,
                color: Colors.grey,
                fontSize: 10,
              ),
            ),
            SizedBox(height: 5),
            Row(
              children: [
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(7),
                    gradient: LinearGradient(
                      colors: <Color>[
                        Theme.of(context).hoverColor,
                        Theme.of(context).primaryColor
                      ],
                    ),
                  ),
                  padding: EdgeInsets.symmetric(horizontal: 7, vertical: 3),
                  child: Image.asset(
                    'assets/title_light.png',
                    width: 25,
                    color: Colors.white,
                  ),
                ),
                SizedBox(width: 5),
                Text(
                  'EGP ${meal.plusPrice}',
                  style: TextStyle(color: Colors.black, fontSize: 13),
                ),
              ],
            ),
            SizedBox(height: 10),
            Text(
              meal.name,
              style: TextStyle(
                color: Colors.black,
                fontSize: 14,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 5),
            Text(
              meal.restaurant,
              style: TextStyle(
                color: Colors.grey,
                fontSize: 12,
              ),
            )
          ],
        ),
      ),
    );
  }
}
