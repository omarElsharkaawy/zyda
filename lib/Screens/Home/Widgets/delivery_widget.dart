import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class DeliveryWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Image.asset('assets/icn_delivery.png'),
        SizedBox(width: 5),
        Text('Delivery to', style: TextStyle(fontSize: 15)),
        SizedBox(width: 5),
        Text(
          'STC',
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
        ),
        SizedBox(width: 5),
        SvgPicture.asset(
          'assets/icn_drop.svg',
          width: 25,
        ),
      ],
    );
  }
}
