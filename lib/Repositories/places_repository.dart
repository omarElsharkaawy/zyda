import 'package:zyda/Helpers/network_helper.dart';
import 'package:zyda/Models/place_model.dart';
import 'package:zyda/Models/prediction_model.dart';

class PlacesRepository {
  static Future<PredictionModel> getPredictions(String input) async {
    Map<String, dynamic> _params = {
      'key': 'AIzaSyB-PBPHzbYC90YVPMuEaM7cCMpEhayuZ4I',
      'input': input,
      'language': 'en',
      'components': 'country:eg',
      'type': 'establishment'
    };
    return NetworkHelper.internal().get(
      PredictionModel(),
      endPoint: null,
      query: _params,
      newUrl: 'https://maps.googleapis.com/maps/api/place/autocomplete/json',
    );
  }

  static Future<PlaceModel> getPlace(String placeID) async {
    Map<String, dynamic> _params = {
      'key': 'AIzaSyB-PBPHzbYC90YVPMuEaM7cCMpEhayuZ4I',
      'place_id': placeID,
      'language': 'en',
    };
    return NetworkHelper.internal().get(
      PlaceModel(),
      endPoint: null,
      query: _params,
      newUrl: 'https://maps.googleapis.com/maps/api/place/details/json',
    );
  }
}
