import 'package:zyda/Helpers/network_helper.dart';
import 'package:zyda/Models/home_model.dart';

class HomeRepository {
  static Future<HomeModel> getHome() async {
    return NetworkHelper.internal().get(HomeModel(),
        endPoint: null, newUrl: 'http://demo4833373.mockable.io/menu');
  }
}
