import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';

class TextHelper {
  String formatDate({@required DateTime date}) {
    return "${date.year.toString().padLeft(4, '0')}-${date.month.toString().padLeft(2, '0')}-${date.day.toString().padLeft(2, '0')}";
  }

  String formatTime({@required DateTime time}) {
    String _formattedTime = DateFormat.jm().format(time);
    return _formattedTime;
  }

  String formatDateTime({@required  DateTime date}) {
    String _formattedTime = DateFormat.Hms().format(date);
    String _formattedDate = formatDate(date: date);
    return _formattedDate + ' ' + _formattedTime;
  }

  String name({String name}) {
    if (name.length < 10) {
      return name;
    } else {
      return ' .. ' + name.substring(0, 10);
    }
  }

  String nameProfile({String name}) {
    if (name.length < 20) {
      return name;
    } else {
      return ' .. ' + name.substring(0, 20);
    }
  }

  String paresHTML({String htmlText}) {
    RegExp exp = RegExp(
        r"<[^>]*>",
        multiLine: true,
        caseSensitive: true
    );

    return htmlText.replaceAll(exp, '');
  }
}
