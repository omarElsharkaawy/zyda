import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';

import 'mappable.dart';

class NetworkHelper {
  static final NetworkHelper _instance = new NetworkHelper.internal();

  NetworkHelper.internal();

  factory NetworkHelper() => _instance;

  Dio _dio = Dio();

  Future<ResponseType> get<ResponseType extends Mappable>(
      ResponseType responseType,
      {@required String endPoint,
      Map<String, dynamic> query,
      Map headers,
      String newUrl}) async {
    var _response;
    _dio.options.baseUrl = '';
    try {
      if (newUrl != null) {
        print(query);
        print(newUrl);
        _response = await _dio.get(
          newUrl,
          options: Options(headers: headers),
          queryParameters: query,
        );
      } else {
        print(query);
        _response = await _dio.get(
          endPoint,
          options: Options(headers: headers),
          queryParameters: query,
        );
      }
    } on DioError catch (e) {
      if (e.response != null) {
        _response = e.response;
      }
    }
    print("Request >>> " + _response.toString());
    return Mappable(responseType, _response.toString()) as ResponseType;
  }

  Future<ResponseType> post<ResponseType extends Mappable>(
      ResponseType responseType,
      {String endPoint,
      Map headers,
      FormData body,
      String newUrl}) async {
    var _response;
    _dio.options.baseUrl = '';
    try {
      print(body);
      if (newUrl != null) {
        print(newUrl);
        _response = await _dio.post(newUrl,
            data: body, options: Options(headers: headers));
      } else {
        _response = await _dio.post(endPoint,
            data: body, options: Options(headers: headers));
      }
    } on DioError catch (e) {
      if (e.response != null) {
        _response = e.response;
      }
    }
    print("Request >>> " + _response.toString());
    return Mappable(responseType, _response.toString()) as ResponseType;
  }
}
