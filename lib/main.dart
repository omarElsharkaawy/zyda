import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:zyda/BLoCs/home_bloc.dart';
import 'package:zyda/BLoCs/places_bloc.dart';

import 'Screens/Register/register.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
      statusBarColor: Colors.transparent,
    ));
    return MultiBlocProvider(
      providers: [
        BlocProvider<PlacesBloc>(
          create: (BuildContext context) => PlacesBloc(),
        ),
        BlocProvider<HomeBloc>(
          create: (BuildContext context) => HomeBloc(),
        ),
      ],
      child: MaterialApp(
        title: 'ZYDA',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          hoverColor: Color(0xffFA5457),
          accentColor: Color(0xff01B4BC),
          primaryColor: Color(0xffCF2E73),
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: Register(),
      ),
    );
  }
}

