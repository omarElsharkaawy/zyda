import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class AppLoader extends StatelessWidget {
  final String label;

  const AppLoader({Key key, this.label}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SpinKitFadingCircle(
          color: Theme.of(context).primaryColor,
          size: 50,
        ),
        SizedBox(height: 10),
        label != null
            ? Text(
                label,
                style: TextStyle(color: Colors.grey),
              )
            : Container()
      ],
    );
  }
}
