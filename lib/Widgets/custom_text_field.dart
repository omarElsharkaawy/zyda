import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {
  final String label;
  final ValueChanged<String> onChange;
  final IconData icon;

  const CustomTextField({Key key, this.label, this.onChange, this.icon})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: Theme.of(context).primaryColor)),
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: TextField(
        onChanged: onChange,
        style: TextStyle(fontSize: 18),
        decoration: InputDecoration(
          border: InputBorder.none,
          suffixIcon: icon != null ? Icon(icon) : null,
          labelText: label,
          labelStyle: TextStyle(fontSize: 15),
        ),
      ),
    );
  }
}
