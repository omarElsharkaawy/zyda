import 'package:flutter/material.dart';

class AppEmpty extends StatelessWidget {
  final String label;

  const AppEmpty({Key key, this.label}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Icon(Icons.hourglass_empty, color: Colors.grey, size: 50),
        SizedBox(height: 10),
        label != null
            ? Text(
                label,
                style: TextStyle(color: Colors.grey),
              )
            : Container()
      ],
    );
  }
}
