import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  final Function onTap;
  final String label;
  final Gradient gradient;
  final Color color;
  final Color borderColor;
  final Color textColor;
  final double radius;
  final double height;
  final double width;

  const CustomButton(
      {Key key,
      @required this.onTap,
      @required this.label,
      this.gradient,
      this.color,
      this.borderColor,
      this.radius,
      this.height,
      this.width,
      this.textColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        width: width ?? 100,
        height: height ?? 50,
        decoration: BoxDecoration(
          color: color ?? Theme.of(context).primaryColor,
          gradient: gradient,
          borderRadius: BorderRadius.circular(radius ?? 5),
          border: Border.all(
            width: 2,
            color: borderColor ?? Colors.transparent,
          ),
        ),
        child: Center(
          child: Text(
            label,
            style: TextStyle(
              color: textColor ?? Colors.white,
              fontWeight: FontWeight.bold
            ),
          ),
        ),
      ),
    );
  }
}
