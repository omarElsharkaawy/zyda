import 'package:flutter/material.dart';

class CustomAppBar extends StatelessWidget {
  final String label;
  final Function onTap;
  final int elevation;
  final List<Widget> actions;
  final Color color;
  final Color labelColor;
  final IconData icon;

  const CustomAppBar(
      {Key key,
      @required this.label,
      this.onTap,
      this.elevation,
      this.actions,
      this.color,
      this.labelColor,
      this.icon})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text(
        label,
        style: TextStyle(color: labelColor ?? Theme.of(context).primaryColor),
      ),
      centerTitle: true,
      backgroundColor: color ?? Colors.white,
      elevation: elevation ?? 0,
      leading: IconButton(
        onPressed: onTap ?? (){},
        icon: Icon(icon ?? Icons.arrow_back,
            color: Theme.of(context).primaryColor),
      ),
      actions: actions ?? [],
    );
  }
}
