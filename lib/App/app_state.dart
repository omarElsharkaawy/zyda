import 'package:zyda/Helpers/mappable.dart';

abstract class AppState {}

class Loading extends AppState {}

class Start extends AppState {}

class Error extends AppState {
  String errorMsg;

  Error({this.errorMsg});
}

class Empty extends AppState {}

class Done extends AppState {
  Mappable response;

  Done({this.response});
}

class GetPlaceDone extends AppState {
  Mappable response;

  GetPlaceDone({this.response});
}