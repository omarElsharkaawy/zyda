abstract class AppEvent {}

class Click extends AppEvent {}

class Restart extends AppEvent {}

class GetPlace extends AppEvent {}
