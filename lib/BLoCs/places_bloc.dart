import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rxdart/rxdart.dart';
import 'package:zyda/App/app_event.dart';
import 'package:zyda/App/app_state.dart';
import 'package:zyda/Models/place_model.dart';
import 'package:zyda/Models/prediction_model.dart';
import 'package:zyda/Repositories/places_repository.dart';

class PlacesBloc extends Bloc<AppEvent, AppState> {
  final _input = BehaviorSubject<String>();
  final _placeID = BehaviorSubject<String>();

  PlacesBloc({AppState initialState}) : super(Start());

  Function(String) get updateInput => _input.sink.add;

  Function(String) get updatePlaceID => _placeID.sink.add;

  @override
  Future<void> close() {
    _input.close();
    _placeID.close();
    return super.close();
  }

  @override
  Stream<AppState> mapEventToState(AppEvent event) async* {
    if (event is Restart) {
      yield Start();
    }
    if (event is Click) {
      yield Loading();
      try {
        PredictionModel _res =
            await PlacesRepository.getPredictions(_input.value);
        if (_res.status == 'OK') {
          yield Done(response: _res);
        } else {
          yield Error(errorMsg: 'No Result');
        }
      } catch (e) {
        Fluttertoast.showToast(msg: 'Check your connection');
        add(Restart());
      }
    }
    if (event is GetPlace) {
      yield Loading();
      try {
        PlaceModel _res = await PlacesRepository.getPlace(_placeID.value);
        if (_res.status == 'OK') {
          yield GetPlaceDone(response: _res);
        } else {
          yield Error(errorMsg: 'No Result');
        }
      } catch (e) {
        Fluttertoast.showToast(msg: 'Check your connection');
        add(Restart());
      }
    }
  }
}
