import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:zyda/App/app_event.dart';
import 'package:zyda/App/app_state.dart';
import 'package:zyda/Models/home_model.dart';
import 'package:zyda/Repositories/home_repositroy.dart';

class HomeBloc extends Bloc<AppEvent, AppState> {
  HomeBloc({AppState initialState}) : super(Start());

  @override
  Stream<AppState> mapEventToState(AppEvent event) async* {
    if (event is Restart) {
      yield Start();
    }
    if (event is Click) {
      yield Loading();
      try {
        HomeModel _res = await HomeRepository.getHome();
        if (_res != null) {
          yield Done(response: _res);
        } else {
          yield Error(errorMsg: 'No Result');
        }
      } catch (e) {
        Fluttertoast.showToast(msg: 'Check your connection');
        add(Restart());
      }
    }
  }
}
