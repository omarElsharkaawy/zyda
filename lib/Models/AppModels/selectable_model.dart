class SelectableModel{
  int id;
  String title;
  bool selected;

  SelectableModel({this.id, this.title, this.selected});
}