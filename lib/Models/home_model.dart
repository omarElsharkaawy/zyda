import 'package:zyda/Helpers/mappable.dart';

class HomeModel extends BaseMappable {
  HomeModel({
    this.light,
    this.bestValue,
    this.plus,
  });

  List<Meal> light;
  List<Meal> bestValue;
  List<Meal> plus;

  @override
  Mappable fromJson(Map<String, dynamic> json) => HomeModel(
        light: json["light"] == null
            ? null
            : List<Meal>.from(
                json["light"].map((x) => Meal.fromJson(x))),
        bestValue: json["best_value"] == null
            ? null
            : List<Meal>.from(
                json["best_value"].map((x) => Meal.fromJson(x))),
        plus: json["plus"] == null
            ? null
            : List<Meal>.from(
                json["plus"].map((x) => Meal.fromJson(x))),
      );
}

class Meal {
  Meal({
    this.img,
    this.regularPrice,
    this.plusPrice,
    this.name,
    this.restaurant,
  });

  String img;
  int regularPrice;
  int plusPrice;
  String name;
  String restaurant;

  factory Meal.fromJson(Map<String, dynamic> json) => Meal(
        img: json["img"] == null ? null : json["img"],
        regularPrice:
            json["regular_price"] == null ? null : json["regular_price"],
        plusPrice: json["plus_price"] == null ? null : json["plus_price"],
        name: json["name"] == null ? null : json["name"],
        restaurant: json["restaurant"] == null ? null : json["restaurant"],
      );
}
